package com.margin;

import com.margin.my.datastructure.MyStack;
import org.junit.Test;

import java.util.Random;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;

public class MyStackTest {

    @Test
    public void successfulPush(){
        final Integer lastIndex = 7;
        final MyStack<Integer> myStack = TestHelper.create(lastIndex);
        assertEquals(myStack.peek(), lastIndex);
    }

    @Test
    public void successfulPeek(){
        final Integer lastIndex = 7;
        final MyStack<Integer> myStack = TestHelper.create(lastIndex);
        assertEquals(myStack.peek(), lastIndex);
        assertEquals(myStack.pop(), lastIndex);
        assertEquals(myStack.peek(), new Integer(lastIndex - 1));
    }

    @Test
    public void successfulPop(){
        final Integer lastIndex = 7;
        final MyStack<Integer> myStack = TestHelper.create(lastIndex);
        assertEquals(myStack.peek(), lastIndex);
        assertEquals(myStack.pop(), lastIndex);
        assertEquals(myStack.peek(), new Integer(lastIndex - 1));
        assertEquals(myStack.pop(), new Integer(lastIndex - 1));
        assertEquals(myStack.peek(), new Integer(lastIndex - 2));
        assertEquals(myStack.pop(), new Integer(lastIndex - 2));
    }

    @Test
    public void successfulArraySizeIncrease(){
        final Integer lastIndex = 15;
        final MyStack<Integer> myStack = TestHelper.create(lastIndex);
        TestHelper.pushToStack(myStack, 4, lastIndex);
        assertEquals(myStack.peek(), new Integer(lastIndex + 4));
    }

    @Test
    public void successfulArraySizeDecrease(){
        final Integer lastIndex = 200000;
        final MyStack<Integer> myStack = TestHelper.create(lastIndex);
        TestHelper.pushToStack(myStack, lastIndex, 1);
        TestHelper.popFromStack(myStack, lastIndex - 2);
        myStack.pop();
    }

    @Test
    public void successfulStressTestMyStack(){
        final Integer lastIndex = 20;
        final MyStack<Integer> myStack = TestHelper.create(lastIndex);
        final ExecutorService executorService = Executors.newFixedThreadPool(10);
        final Random random = new Random(100);
        for (int i = 0; i < 600000; i++) {
            final int current = i;
            executorService.execute(() -> {
                if(random.nextInt() %2 == 0){
                    myStack.push(Integer.valueOf(current));
                    myStack.pop();
                } else {
                    myStack.push(Integer.valueOf(current));
                }
            });
        }
    }

    @Test
    public void successfulStressTestJavaStack(){
        final Integer lastIndex = 20;
        final Stack<Integer> stack = TestHelper.createJavaStack(lastIndex);
        final ExecutorService executorService = Executors.newFixedThreadPool(10);
        final Random random = new Random(100);
        for (int i = 0; i < 600000; i++) {
            final int current = i;
            executorService.execute(() -> {
                if(random.nextInt() %2 == 0){
                    stack.push(Integer.valueOf(current));
                    stack.pop();
                } else {
                    stack.push(Integer.valueOf(current));
                }
            });
        }
    }
}
