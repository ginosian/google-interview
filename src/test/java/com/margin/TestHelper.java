package com.margin;

import com.margin.my.datastructure.MyStack;

import java.util.Stack;

public class TestHelper {

    public static MyStack<Integer> create(final int lastIndex){
        final MyStack<Integer> myStack = new MyStack<>();
        for (int i = 1; i <= lastIndex; i++) {
            myStack.push(i);
        }
        return myStack;
    }

    public static Stack<Integer> createJavaStack(final int lastIndex){
        final Stack<Integer> stack = new Stack<>();
        for (int i = 1; i <= lastIndex; i++) {
            stack.push(i);
        }
        return stack;
    }

    public static void popFromStack(final MyStack stack, final int count){
        for (int i = 0; i < count + count; i++) {
            stack.pop();
        }
    }

    public static void pushToStack(final MyStack stack, final int count, final int startFrom){
        for (int i = startFrom; i <= startFrom + count; i++) {
            stack.push(i);
        }
    }

    public static void popFromJavaStack(final Stack stack, final int count){
        for (int i = 0; i < count + count; i++) {
            stack.pop();
        }
    }

    public static void pushToJavaStack(final Stack stack, final int count, final int startFrom){
        for (int i = startFrom; i <= startFrom + count; i++) {
            stack.push(i);
        }
    }
}
