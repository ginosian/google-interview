package com.margin.my.datastructure;

public class MyStack<T> {
    private int lastIndex;
    private int maxSize;
    private int initialSize;
    private Object[] data;

    public MyStack() {
        lastIndex = -1;
        initialSize = 16;
        maxSize = initialSize;
        data = new Object[maxSize];
    }

    public synchronized void push(final T input){

        final int nextLastIndex = lastIndex + 1;
        if(maxSize - 1 < nextLastIndex){
            increaseDataStorage();
        }
        lastIndex = nextLastIndex;
        data[nextLastIndex] = input;
    }

    public synchronized T peek(){
        if(lastIndex <= 0){
            return null;
        }
        final Object result = data[lastIndex];
        return (T) result;
    }

    public synchronized T pop(){
        if(lastIndex <= 0){
            return null;
        }
        final int nextLastIndex = lastIndex - 1;
        if(maxSize - nextLastIndex > initialSize + nextLastIndex){
            decreaseDataStorage();
        }
        final Object result = data[lastIndex];
        data[lastIndex] = null;
        lastIndex = nextLastIndex;
        return (T) result;
    }

    private void increaseDataStorage(){
        final int nextSize = maxSize + maxSize;
        recalculateDataSize(nextSize);
        maxSize = nextSize;
    }

    private void decreaseDataStorage(){
        final int nextSize = lastIndex + initialSize;
        recalculateDataSize(nextSize);
        maxSize = nextSize;
    }

    private void recalculateDataSize(final int nextSize){
        final Integer[] newData = new Integer[nextSize];
        /**
         * For example System.arraycopy(source, 3, destination, 2, 5) will copy 5 elements from source to destination,
         * beginning from 3rd index of source and appending to 2nd index of destination.
         * */
        System.arraycopy(data, 0, newData, 0, lastIndex + 1);
        data = newData;
    }
}
